<?php

namespace Drupal\search_exclude_nid\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The form to collect nids to be excluded.
 */
class SearchExcludeNidForm extends ConfigFormBase {

  /**
   * The field storage config storage.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Creates a SearchExcludeNidForm form.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'search_exclude_nid_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form constructor.
    $form = parent::buildForm($form, $form_state);
    // Default settings.
    $config = $this->config('search_exclude_nid.settings');
    $excluded_nids = $config->get('excluded_nids');

    // Source text field.
    $form['excluded_nids'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Nodes to be excluded'),
      '#default_value' => !empty($excluded_nids) ? implode(',', $excluded_nids) : '',
      '#description' => $this->t('Comma separated list of node ids (node:nid) to be excluded, eg: 1,4,8,23 etc.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('search_exclude_nid.settings');

    $excluded_nids = [];

    if (!empty($form_state->getValue('excluded_nids'))) {
      $excluded_nids_arr = explode(',', $form_state->getValue('excluded_nids'));
      foreach ($excluded_nids_arr as $excluded_nid) {
        $excluded_nid = intval($excluded_nid);
        $node = $this->entityTypeManager->getStorage('node')->load($excluded_nid);

        // Check if node exists for given nid and avoid duplicates.
        if ($excluded_nid && !in_array($excluded_nid, $excluded_nids) && !empty($node)) {
          $excluded_nids[] = $excluded_nid;
        }
        else {
          $this->messenger()->addWarning($this->t('nid: %nid has been removed from exclusion list as no node exists with that id or it is a duplicate.', ['%nid' => $excluded_nid]));
        }
      }
    }

    $config->set('excluded_nids', $excluded_nids);
    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'search_exclude_nid.settings',
    ];
  }

}
